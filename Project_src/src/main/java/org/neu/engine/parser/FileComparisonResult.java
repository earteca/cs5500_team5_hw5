package org.neu.engine.parser;

import java.util.ArrayList;
import java.util.List;

public class FileComparisonResult {
    private String filename1;
    private String filename2;
    private List<LineNumberPair> file1LineNumber = new ArrayList<>();
    private List<LineNumberPair> file2LineNumber = new ArrayList<>();

    public FileComparisonResult(String filename1, String filename2) {
        this.filename1 = filename1;
        this.filename2 = filename2;
    }

    public String getFilename1() {
        return filename1;
    }

    public void setFilename1(String filename1) {
        this.filename1 = filename1;
    }

    public String getFilename2() {
        return filename2;
    }

    public void setFilename2(String filename2) {
        this.filename2 = filename2;
    }

    public List<LineNumberPair> getFile1LineNumber() {
        return file1LineNumber;
    }

    public void setFile1LineNumber(List<LineNumberPair> file1LineNumber) {
        this.file1LineNumber = file1LineNumber;
    }

    public List<LineNumberPair> getFile2LineNumber() {
        return file2LineNumber;
    }

    public void setFile2LineNumber(List<LineNumberPair> file2LineNumber) {
        this.file2LineNumber = file2LineNumber;
    }
}
