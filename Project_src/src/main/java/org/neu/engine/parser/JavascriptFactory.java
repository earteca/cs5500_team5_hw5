package org.neu.engine.parser;

public class JavascriptFactory implements AbstractFactory {

    @Override
    public CustomParser getParser() {
        return new JavascriptParser();
    }

    @Override
    public ASTTravelor getASTTraveler() {
        return new JavascriptASTTraveller();
    }
}
