package org.neu.engine.parser;

public class PythonFactory implements AbstractFactory {
    @Override
    public CustomParser getParser() {
        return new PythonParser();
    }

    @Override
    public ASTTravelor getASTTraveler() {
        return new PythonASTTraveler();
    }
}
