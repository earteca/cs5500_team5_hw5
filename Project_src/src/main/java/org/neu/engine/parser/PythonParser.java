package org.neu.engine.parser;

import com.example.Python3Lexer;
import com.example.Python3Parser;
import org.antlr.v4.runtime.*;

import java.io.IOException;

public class PythonParser implements CustomParser {

    private Lexer lexer;
    private CommonTokenStream tokens;
    private Python3Parser parser;
    @Override
    public RuleContext getASTRoot(String filePath) {
        try {
            CharStream text = CharStreams.fromFileName(filePath);
            this.lexer = new Python3Lexer(text);
            this.tokens = new CommonTokenStream(lexer);
            this.parser = new Python3Parser(tokens);
            Python3Parser.File_inputContext fileInputContext = parser.file_input();
            return fileInputContext;
        } catch (IOException e) {
            return null;
        }
    }

    public Lexer getLexer() {
        return lexer;
    }

    public void setLexer(Lexer lexer) {
        this.lexer = lexer;
    }

    public CommonTokenStream getTokens() {
        return tokens;
    }

    public void setTokens(CommonTokenStream tokens) {
        this.tokens = tokens;
    }

    public Parser getParser() {
        return parser;
    }

    public void setParser(Parser parser) {
        this.parser = (Python3Parser) parser;
    }
}
