package org.neu.engine.parser;

import com.example.ECMAScriptParser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.tree.ParseTree;

public class JavascriptASTTraveller implements ASTTravelor  {

    private String ans="";
    private void dfs(RuleContext node) {
        boolean shouldBeIgnored = node.getChildCount() == 1 && node.getChild(0) instanceof ParserRuleContext;
        if(!shouldBeIgnored) {
            String ruleName = ECMAScriptParser.ruleNames[node.getRuleIndex()];
            ans+=ruleName+" ";
        }
        for (int i=0;i<node.getChildCount();i++) {
            ParseTree element = node.getChild(i);
            if (element instanceof RuleContext) {
                dfs((RuleContext)element);
            }
        }
    }

    @Override
    public String getString(RuleContext node) {
       ans = "";
       this.dfs(node);
       return this.ans;
    }
}
