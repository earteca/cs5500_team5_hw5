package org.neu.engine.parser;

public class LineNumberPair {
    private int startLineNumber;
    private int lastLineNumber;

    public LineNumberPair(int startLineNumber, int lastLineNumber) {
        this.startLineNumber = startLineNumber;
        this.lastLineNumber = lastLineNumber;
    }

    public int getStartLineNumber() {
        return startLineNumber;
    }

    public void setStartLineNumber(int startLineNumber) {
        this.startLineNumber = startLineNumber;
    }

    public int getLastLineNumber() {
        return lastLineNumber;
    }

    public void setLastLineNumber(int lastLineNumber) {
        this.lastLineNumber = lastLineNumber;
    }
}
