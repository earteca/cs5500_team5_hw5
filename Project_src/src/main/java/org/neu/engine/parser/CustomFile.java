package org.neu.engine.parser;

import com.example.ECMAScriptParser;
import com.example.Python3Lexer;
import com.example.Python3Parser;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.ArrayList;
import java.util.List;

public class CustomFile {
    public static final String uploadDirectory = System.getProperty("user.dir") + "/submissions/";
    private String fileName;
    private RuleContext astRoot;
    private List<RuleContext> functionNodes = new ArrayList<>();
    private Lexer fileLexer;
    private CommonTokenStream fileTokens;
    private Parser fileParser;
    AbstractFactory factory;

    public CustomFile(String fileName, AbstractFactory factory) {
        this.fileName = fileName;
        this.factory = factory;
        this.generateAST(this.factory);
        this.storeAllFunctionNode(astRoot);
    }

    public void generateAST(AbstractFactory factory) {
        CustomParser parser = factory.getParser();
        astRoot = parser.getASTRoot(uploadDirectory+fileName);
        fileLexer = parser.getLexer();
        fileTokens = parser.getTokens();
        fileParser = parser.getParser();
    }

    private void storeAllFunctionNode(RuleContext node) {
        String ruleName = "";
        if(this.fileParser instanceof Python3Parser)
            ruleName = Python3Parser.ruleNames[node.getRuleIndex()];
        else if(this.fileParser instanceof ECMAScriptParser)
            ruleName = ECMAScriptParser.ruleNames[node.getRuleIndex()];
        if(ruleName == "funcdef") {
            functionNodes.add(node);
        }
        else if(ruleName == "functionBody") {
            functionNodes.add(node);
        }
        else {
            for(int i=0; i<node.getChildCount(); i++) {
                ParseTree element = node.getChild(i);
                if(element instanceof RuleContext) {
                    this.storeAllFunctionNode((RuleContext) element);
                }
            }
        }
    }

    public int getStartLineNumber(RuleContext node) {
        Interval interval = node.getSourceInterval();
        Token firstToken = fileTokens.get(interval.a);
        return firstToken.getLine();
    }

    public int getLastLineNumber(RuleContext node) {
        Interval interval = node.getSourceInterval();
        Token lastToken = fileTokens.get(interval.b);
        return lastToken.getLine();
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public RuleContext getAstRoot() {
        return astRoot;
    }

    public void setAstRoot(RuleContext astRoot) {
        this.astRoot = astRoot;
    }

    public List<RuleContext> getFunctionNodes() {
        return functionNodes;
    }

    public void setFunctionNodes(List<RuleContext> functionNodes) {
        this.functionNodes = functionNodes;
    }

    public Lexer getFileLexer() {
        return fileLexer;
    }

    public void setFileLexer(Python3Lexer fileLexer) {
        this.fileLexer = fileLexer;
    }

    public CommonTokenStream getFileTokens() {
        return fileTokens;
    }

    public void setFileTokens(CommonTokenStream fileTokens) {
        this.fileTokens = fileTokens;
    }

    public Parser getFileParser() {
        return fileParser;
    }

    public void setFileParser(Python3Parser fileParser) {
        this.fileParser = fileParser;
    }
}
