package org.neu.engine.parser;

import org.antlr.v4.runtime.RuleContext;
import org.neu.algorithms.ICompareString;
import org.neu.algorithms.LongestCommonSubSequencer;
import org.neu.algorithms.RatcliffObershelp;
import org.neu.algorithms.SorensenDice;
import org.neu.mvc.models.Results;
import org.neu.mvc.models.Submission;

import java.util.ArrayList;
import java.util.List;

public class Comparator {
  private ASTTravelor astTraveler;
  private List<ICompareString> iCompareStringList = new ArrayList<>();

  public Comparator(AbstractFactory factory) {
    this.astTraveler = factory.getASTTraveler();
    this.iCompareStringList.add(new LongestCommonSubSequencer());
    this.iCompareStringList.add(new RatcliffObershelp());
    this.iCompareStringList.add(new SorensenDice());
  }

  public Results compareSubmission(Submission sub1, Submission sub2) {
    List<CustomFile> filesList1 = sub1.getCustomFilesList();
    List<CustomFile> filesList2 = sub2.getCustomFilesList();
    List<FileComparisonResult> fileComparisonResults = new ArrayList<>();
    Results results = new Results(sub1.getStudentId(), sub2.getStudentId());
    for (int i = 0; i < filesList1.size(); i++) {
      for (int j = 0; j < filesList2.size(); j++) {
        CustomFile file1 = filesList1.get(i);
        CustomFile file2 = filesList2.get(j);
        fileComparisonResults.add(compareFile(file1, file2));
      }
    }
    results.setFileComparisonResults(fileComparisonResults);
    return results;
  }

    private FileComparisonResult compareFile(CustomFile file1, CustomFile file2) {
        List<RuleContext> functionList1 = file1.getFunctionNodes();
        List<RuleContext> functionList2 = file2.getFunctionNodes();
        FileComparisonResult fileComparisonResult =
                new FileComparisonResult(file1.getFileName(),file2.getFileName());
        List<LineNumberPair> file1List = new ArrayList<>();
        List<LineNumberPair> file2List = new ArrayList<>();
        for(int i=0; i < functionList1.size(); i++) {
            for(int j=0; j < functionList2.size(); j++) {
                RuleContext function1 = functionList1.get(i);
                RuleContext function2 = functionList2.get(j);
                if(compareFunction(function1,function2)) {
                    file1List.add(getLineNumberPair(file1,function1));
                    file2List.add(getLineNumberPair(file2,function2));
                }
            }
        }
        fileComparisonResult.setFile1LineNumber(file1List);
        fileComparisonResult.setFile2LineNumber(file2List);
        return fileComparisonResult;
    }

    private boolean compareFunction(RuleContext function1, RuleContext function2) {
        String functionTree1 = astTraveler.getString(function1);
        String functionTree2 = astTraveler.getString(function2);
        String functionContent1 = getFunctionContentString(functionTree1);
        String functionContent2 = getFunctionContentString(functionTree2);
        if(stringIsSimilar(functionContent1,functionContent2)) {
            return true;
        }
        return false;
    }

  private String getFunctionContentString(String functionString) {
    String[] stringArr = functionString.split("suite", 2);
    if (stringArr.length == 2) {
      return stringArr[1];
    }
    return functionString;
  }

  private boolean stringIsSimilar(String str1, String str2) {
    for (ICompareString iCompareString : iCompareStringList) {
      iCompareString.compareString(str1, str2);
      if (iCompareString.isPlagarised()) {
        return true;
      }
    }
    return false;
  }

  private LineNumberPair getLineNumberPair(CustomFile file, RuleContext functionNode) {
    int startLine = file.getStartLineNumber(functionNode);
    int lastLine = file.getLastLineNumber(functionNode);
    return new LineNumberPair(startLine, lastLine);
  }
}
