package org.neu.engine.parser;

import org.antlr.v4.runtime.RuleContext;

public interface ASTTravelor {
    /**
     * Get String of Tree where node is root node.
     * The tree is traversed in Pre-order.
     * @param node A RuleContext Node
     * @return
     */
    public String getString(RuleContext node);
}
