package org.neu.engine.parser;

import com.example.ECMAScriptParser;
import com.example.ECMAScriptLexer;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;


public class JavascriptParser implements CustomParser {

    private Lexer lexer;
    private CommonTokenStream tokens;
    private ECMAScriptParser parser;


    @Override
    public RuleContext getASTRoot(String filePath)  {
        try {
            CharStream text = CharStreams.fromFileName(filePath);
             lexer = new ECMAScriptLexer(text);
             tokens = new CommonTokenStream(lexer);
             parser = new ECMAScriptParser(tokens);
            ECMAScriptParser.ProgramContext programContext = parser.program();
            RuleContext ruleContext = programContext.getRuleContext();
            return ruleContext;
        }catch(IOException ie) {
            return null;
        }
    }

    @Override
    public Lexer getLexer() {
        return this.lexer;
    }

    @Override
    public void setLexer(Lexer lexer) {
        this.lexer = lexer;
    }

    @Override
    public CommonTokenStream getTokens() {
        return this.tokens;
    }

    @Override
    public void setTokens(CommonTokenStream tokens) {
        this.tokens = tokens;
    }

    @Override
    public Parser getParser() {
        return this.parser;
    }

    @Override
    public void setParser(Parser parser) {
        this.parser = (ECMAScriptParser) parser;
    }
}
