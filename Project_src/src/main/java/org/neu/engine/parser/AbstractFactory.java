package org.neu.engine.parser;

public interface AbstractFactory {
    public CustomParser getParser();
    public ASTTravelor getASTTraveler();
}
