package org.neu.engine.parser;

import com.example.Python3Lexer;
import com.example.Python3Parser;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;

public interface CustomParser {

    /**
     * Returns the root of AST generated for file.
     * @param filePath Path to the File
     * @return A RuleContext Root Node
     */
    public RuleContext getASTRoot(String filePath);

    public Lexer getLexer();

    public void setLexer(Lexer lexer);

    public CommonTokenStream getTokens();

    public void setTokens(CommonTokenStream tokens);

    public Parser getParser();

    public void setParser(Parser parser);
}
