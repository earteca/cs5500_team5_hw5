package org.neu;

import org.neu.mvc.controllers.SubmissionController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;

@SpringBootApplication
public class PlagiarismSoftware {

    public static void main(String[] args) {
        new File(SubmissionController.uploadDirectory).mkdirs();
        SpringApplication.run(PlagiarismSoftware.class, args);
    }

}
