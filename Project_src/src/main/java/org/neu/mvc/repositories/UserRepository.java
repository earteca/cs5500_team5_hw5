package org.neu.mvc.repositories;

import org.neu.mvc.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends MongoRepository<User, String> {

    /**
     * Find a user by username and password
     * @param username username
     * @param password password
     * @return the associated User
     */
    User findUserByUsernameAndPassword(String username, String password);
}
