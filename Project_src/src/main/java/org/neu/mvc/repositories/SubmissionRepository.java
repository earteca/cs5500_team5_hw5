package org.neu.mvc.repositories;

import org.bson.types.ObjectId;
import org.neu.mvc.models.Submission;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SubmissionRepository extends MongoRepository<Submission, String> {

    Submission findDistinctFirstBy(ObjectId studentID);

    Optional<Submission> findById(ObjectId objectId);

    List<Submission> findAllByAssignmentId(ObjectId assignmentId);
}
