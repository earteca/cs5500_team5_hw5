package org.neu.mvc.repositories;

import org.neu.mvc.models.Results;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResultRepository extends MongoRepository<Results, String> {
}
