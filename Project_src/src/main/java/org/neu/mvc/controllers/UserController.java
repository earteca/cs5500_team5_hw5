package org.neu.mvc.controllers;

import org.neu.mvc.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.neu.mvc.repositories.UserRepository;

@RestController
@CrossOrigin(origins = {"*"}, allowCredentials = "true", allowedHeaders = "*")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @PostMapping("/users/login")
    public User login(@RequestBody User user) {
        return userRepository.findUserByUsernameAndPassword(user.getUsername(), user.getPassword());
    }

    @GetMapping("/users/test")
    public String restAccessTest() {
        return "Working";
    }
}
