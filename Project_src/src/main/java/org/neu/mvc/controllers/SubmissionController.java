package org.neu.mvc.controllers;

import org.bson.types.ObjectId;
import org.neu.engine.parser.AbstractFactory;
import org.neu.engine.parser.Comparator;
import org.neu.engine.parser.JavascriptFactory;
import org.neu.engine.parser.PythonFactory;
import org.neu.mvc.models.Results;
import org.neu.mvc.models.Submission;
import org.neu.mvc.repositories.ResultRepository;
import org.neu.mvc.repositories.SubmissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = {"*"}, allowCredentials = "true", allowedHeaders = "*")
public class SubmissionController {

  public static final String uploadDirectory = System.getProperty("user.dir") + "/submissions/";

  @Autowired
  private SubmissionRepository submissionRepository;
  @Autowired
  private ResultRepository resultRepository;

  @PostMapping("/assignment/{assignmentId}/submission/")
  public Submission submit(@PathVariable("assignmentId") ObjectId assignmentId,
                           @RequestPart("uploadingFiles") MultipartFile[] uploadingFiles,
                           @RequestPart(value = "submission", required = false) Submission submission) throws IOException {
    Submission submissions = null;
    if (submission == null) {
      submissions = new Submission();
    } else {
      submissions = submission;
    }
    submissions.setId(ObjectId.get());
    submissions.setAssignmentId(assignmentId);
    List<String> uploadedFileNames = new ArrayList<>();
    int fileCounter = 0;
    for (MultipartFile uploadedFile : uploadingFiles) {
      String[] fileNameSplits = uploadedFile.getOriginalFilename().split("\\.");
      String extension = fileNameSplits[fileNameSplits.length - 1];
      File file = new File(uploadDirectory + submissions.getStudentId() + "-"
              + submissions.getId() + "-" + fileCounter + "." + extension);
      uploadedFileNames.add(file.getName());
      uploadedFile.transferTo(file);
      fileCounter++;
    }
    submissions.setFileNames(uploadedFileNames);
    submissionRepository.save(submissions);
    return submissions;
  }

  @GetMapping("/submissions/{Id}")
  public Submission getSubmissions(@PathVariable("Id") ObjectId objectId) {
    Optional<Submission> sub2 = submissionRepository.findById(objectId);
    if (sub2.isPresent()) {
        Submission tempsub = sub2.get();
        runComparisonVsAll(tempsub);
        Submission sub = new Submission();
        return sub;
    }
    return null;
  }

  @GetMapping("/submissions/{id}/files/{fileName}")
  public File getFileByName(@PathVariable("id") ObjectId id, @PathVariable("fileName") String fileName) {
    Submission submission = submissionRepository.findById(id).get();
    File file = null;
    if (submission.getFileNames().contains(fileName)) {
      file = new File(uploadDirectory + fileName);
    }
    return file;
  }


  private void runComparisonVsAll(Submission submission) {
    ObjectId assignmentId = submission.getAssignmentId();
    boolean isPython = submission.isPython();
    List<Submission> submissionsList = submissionRepository.findAllByAssignmentId(assignmentId);
    AbstractFactory abstractFactory = null;
    if(isPython) {
      abstractFactory = new PythonFactory();
    } else {
      abstractFactory =  new JavascriptFactory();
    }
    submission.generateCustomFile(abstractFactory);
    Comparator comparator = new Comparator(abstractFactory);
    for (Submission submission1 : submissionsList) {
      submission1.generateCustomFile(abstractFactory);
      System.out.println(submission.getStudentId());
      System.out.println(submission.getId());
      if (!submission1.getStudentId().equals(submission.getStudentId())) {
        Results results = comparator.compareSubmission(submission, submission1);
        resultRepository.save(results);
      }
    }
  }
}
