package org.neu.mvc.controllers;

import org.neu.mvc.repositories.AssignmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = {"*"}, allowCredentials = "true", allowedHeaders = "*")
public class AssignmentController {

    @Autowired
    private AssignmentRepository assignmentRepository;

}
