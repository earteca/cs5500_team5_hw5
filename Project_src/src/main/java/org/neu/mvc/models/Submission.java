package org.neu.mvc.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.neu.engine.parser.AbstractFactory;
import org.neu.engine.parser.CustomFile;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "submissions")
@JsonIgnoreProperties(value = {"createdAt"}, allowGetters = true)
public class Submission {

  final static Logger logger = Logger.getLogger(User.class);

  @Id
  private ObjectId objectID;
  private ObjectId studentId;
  private ObjectId assignmentId;
  private List<String> fileNames;
  private List<CustomFile> customFilesList = new ArrayList<>();
  private boolean isPython = false;

  public boolean isPython() {
    return isPython;
  }

  public void setPython(boolean python) {
    isPython = python;
  }

  public Submission() {

  }

  public Submission(ObjectId studentID) {
    this.studentId = studentID;
  }

  public List<String> getFileNames() {
    return fileNames;
  }

  public void setFileNames(List<String> fileNames) {
    this.fileNames = fileNames;
  }

  public ObjectId getAssignmentId() {
    return assignmentId;
  }

  public void setAssignmentId(ObjectId assignmentId) {
    this.assignmentId = assignmentId;
  }

  public ObjectId getId() {
    return objectID;
  }

  public void setId(ObjectId objectID) {
    this.objectID = objectID;
  }

  public ObjectId getStudentId() {
    return studentId;
  }

  public void setStudentId(ObjectId student_id) {
    this.studentId = student_id;
  }

  public List<CustomFile> getCustomFilesList() {
    return customFilesList;
  }

  public void setCustomFilesList(List<CustomFile> customFilesList) {
    this.customFilesList = customFilesList;
  }

  public void generateCustomFile(AbstractFactory abstractFactory) {
    for (String fileName : fileNames) {
      CustomFile customFile = new CustomFile(fileName, abstractFactory);
      customFilesList.add(customFile);
    }
  }


}
