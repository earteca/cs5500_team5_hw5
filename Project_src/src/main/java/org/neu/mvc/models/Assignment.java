package org.neu.mvc.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "assignments")
@JsonIgnoreProperties(value = {"createdAt"}, allowGetters = true)
public class Assignment {

  final static Logger logger = Logger.getLogger(User.class);

  @Id
  private ObjectId id;
  private String title;
  private String description;
  private User createdBy;

  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public User getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(User createdBy) {
    this.createdBy = createdBy;
  }
}
