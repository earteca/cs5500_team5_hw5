package org.neu.mvc.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.neu.engine.parser.FileComparisonResult;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "results")
@JsonIgnoreProperties(value = {"createdAt"}, allowGetters = true)
public class Results {
    final static Logger logger = Logger.getLogger(Results.class);

    @Id
    private ObjectId objectID;
    private ObjectId student1;
    private ObjectId student2;
    private List<FileComparisonResult> fileComparisonResults = new ArrayList<>();

    public Results(ObjectId student1, ObjectId student2) {
        this.student1 = student1;
        this.student2 = student2;
    }

    public ObjectId getId() {
        return objectID;
    }

    public void setId(ObjectId id) {
        objectID = id;
    }

    public ObjectId getStudent1() {
        return student1;
    }

    public void setStudent1(ObjectId student1) {
        this.student1 = student1;
    }

    public ObjectId getStudent2() {
        return student2;
    }

    public void setStudent2(ObjectId student2) {
        this.student2 = student2;
    }

    public List<FileComparisonResult> getFileComparisonResults() {
        return fileComparisonResults;
    }

    public void setFileComparisonResults(List<FileComparisonResult> fileComparisonResults) {
        this.fileComparisonResults = fileComparisonResults;
    }
}
