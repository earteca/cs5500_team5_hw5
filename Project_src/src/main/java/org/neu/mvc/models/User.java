package org.neu.mvc.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "users")
@JsonIgnoreProperties(value = {"createdAt"}, allowGetters = true)
public class User {

  final static Logger logger = Logger.getLogger(User.class);

  @Id
  private ObjectId objectID;
  private String username;
  private String password;

  public User(ObjectId objectID, String username, String password) {
    this.objectID = objectID;
    this.password = password;
    this.username = username;
  }

  public User() {
  }

  public ObjectId getId() {
    return objectID;
  }

  public void setId(ObjectId objectID) {
    this.objectID = objectID;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }


}
