package org.neu.algorithms;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class SorensenDice extends AbstractStringComparer {

  public SorensenDice() {
    super();
  }

  private HashMap<String, Integer> buildTokenCountsMap(List<String> tokens) {
    HashMap<String, Integer> tokenCounts = new HashMap<>();
    for (int i = 0; i < tokens.size(); i++) {
      String token = tokens.get(i);
      if (!tokenCounts.containsKey(token)) {
        tokenCounts.put(token, 1);
      } else {
        tokenCounts.put(token, tokenCounts.get(token) + 1);
      }
    }
    return tokenCounts;
  }

  @Override
  public void compareString(String str1, String str2) {

    List<String> tokens1 = Arrays.asList(str1.split(" "));
    List<String> tokens2 = Arrays.asList(str2.split(" "));

    HashMap<String, Integer> tokens1Counts = buildTokenCountsMap(tokens1);
    HashMap<String, Integer> tokens2Counts = buildTokenCountsMap(tokens2);
    HashMap<String, Integer> commonTokenCounts = new HashMap<>();

    for (String key : tokens1Counts.keySet()) {
      if (tokens2Counts.containsKey(key)) {
        commonTokenCounts.put(key, Math.min(tokens1Counts.get(key),tokens2Counts.get(key)));
      }
    }
    int commonCounts = commonTokenCounts.values().stream().mapToInt(i -> i).sum();
    int str1Counts = tokens1Counts.values().stream().mapToInt(i -> i).sum();
    int str2Counts = tokens2Counts.values().stream().mapToInt(i -> i).sum();

    similarityScore = (float) (2 * commonCounts) / (str1Counts + str2Counts);
  }
}
