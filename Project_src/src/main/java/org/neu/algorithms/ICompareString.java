package org.neu.algorithms;

public interface ICompareString {

  /**
   * Compares two strings to see how similar they are
   *
   * @param str1 the first string to compare
   * @param str2 the second string to compare
   */
  public void compareString(String str1, String str2);

  /**
   * Gets a score based on how similar the two strings were
   *
   * @return the score
   */
  public float getSimilarityScore() throws java.lang.IllegalStateException;

  /**
   * Determines if there was plagiarism
   *
   * @return true if so, false otherwise
   */
  public boolean isPlagarised();
}
