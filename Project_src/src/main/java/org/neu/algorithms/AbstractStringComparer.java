package org.neu.algorithms;

public abstract class AbstractStringComparer implements ICompareString {

  protected float similarityScore;

  public AbstractStringComparer() {
    this.similarityScore = -1;
  }

  @Override
  public float getSimilarityScore() throws IllegalStateException {
    if (this.similarityScore == -1) {
      throw new IllegalStateException("Two strings must be compared before a score can be" +
              " retrieved");
    }
    return this.similarityScore;
  }


  @Override
  public boolean isPlagarised() {
    return this.similarityScore >= .70;
  }

}
