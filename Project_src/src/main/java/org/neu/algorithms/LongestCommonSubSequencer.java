package org.neu.algorithms;

import static java.lang.Integer.max;

/**
 * Referenced from https://www.geeksforgeeks.org/longest-common-subsequence-dp-4/
 */
public class LongestCommonSubSequencer extends AbstractStringComparer {

  private String lcs;

  public LongestCommonSubSequencer() {
    this.lcs = null;
  }

  public String getLCS() {
    return this.lcs;
  }


  @Override
  public void compareString(String str1, String str2) {
    String[] X = str1.replaceAll(" ","").split("");
    String[] Y = str2.replaceAll(" ","").split("");

    float C[][] = new float[X.length + 1][Y.length + 1];
    int[] longestCommonSubstringIndex = new int[]{0,0};

    for (int i = 0; i <=X.length; i++) {
      for (int j = 0; j <=Y.length; j++) {
        if (i == 0 || j == 0) {
          C[i][j] = 0;
        } else if (X[i-1].equals(Y[j-1])) {
          C[i][j] = C[i - 1][j - 1] + 1;
          if(C[i][j] > C[longestCommonSubstringIndex[0]][longestCommonSubstringIndex[1]]) {
            longestCommonSubstringIndex[0] = i;
            longestCommonSubstringIndex[1] = j;
          }
        } else {
          C[i][j] = 0;
        }
      }
    }

    int i = longestCommonSubstringIndex[0];
    int j = longestCommonSubstringIndex[1];

    StringBuilder stringBuilder = new StringBuilder();

    while (C[i][j] != 0) {
      if (X[i-1].equals(Y[j-1])) {
        stringBuilder.append(X[i-1]);
        i--;
        j--;
      }
    }
    stringBuilder.reverse();
    this.lcs = stringBuilder.toString();
    similarityScore = (float) C[longestCommonSubstringIndex[0]][longestCommonSubstringIndex[1]] / (float) Math.min(X.length, Y.length);
  }
}
