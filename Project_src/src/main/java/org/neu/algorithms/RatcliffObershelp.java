package org.neu.algorithms;

import java.util.Stack;

import javax.swing.*;

public class RatcliffObershelp extends AbstractStringComparer {

  String baseInput;
  String otherInput;
  float matchCount;

  public RatcliffObershelp(String baseInput) {

    //this.baseInput = baseInput.toUpperCase();
    //super();
  }

  public RatcliffObershelp() {
    matchCount = 0;
    baseInput = "";
    otherInput = "";
  }

  private int compare(Stack<String> baseInputStack, Stack<String> inputStack) {
    String firstString = baseInputStack.pop();
    String otherString = inputStack.pop();

    int compareSize = Math.min(firstString.length(), otherString.length());
    while (compareSize > 0) {
      int position = 0;

      if(position + (compareSize - 1) < firstString.length()) {
        String substring = firstString.substring(position, position + compareSize);
        if (otherString.contains(substring)) {

          String[] firstRest =
                  firstString.split(substring, 2);
          String[] otherRest =
                  otherString.split(substring, 2);

          int newLength = Math.min(firstRest.length, otherRest.length);

          if (newLength > 1) {

            for (int i = 0; i < newLength; i++) {
              if (!"".equals(firstRest[i])) {
                baseInputStack.push(firstRest[i]);
              }
              if (!"".equals(otherRest[i])) {
                inputStack.push(otherRest[i]);
              }
            }
          }
          return compareSize;
        }
        position ++;
      }
      compareSize --;
    }

    return 0;
  }

  @Override
  public void compareString(String str1, String str2) {
    this.baseInput = str1.toUpperCase();
    this.otherInput = str2.toUpperCase();
    matchCount = 0;
    String otherString = str2.toUpperCase();
    Stack<String> inputStack1 = new Stack<>();
    Stack<String> inputStack2 = new Stack<>();

    inputStack1.push(baseInput);
    inputStack2.push(otherString);


    while (inputStack1.size() > 0 && inputStack2.size() > 0) {
      matchCount += compare(inputStack1, inputStack2);
    }
    float percentage = matchCount * 2;
    percentage = percentage / (baseInput.length() + otherInput.length());
    similarityScore  = percentage;
  }
}
