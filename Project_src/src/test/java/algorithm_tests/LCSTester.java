package algorithm_tests;

import org.antlr.v4.runtime.RuleContext;
import org.junit.Assert;
import org.junit.Test;

import org.neu.algorithms.LongestCommonSubSequencer;
import org.neu.engine.parser.ASTTravelor;
import org.neu.engine.parser.CustomFile;
import org.neu.engine.parser.PythonFactory;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class LCSTester {

  private static String testFilesDir = "files_for_test/";

  @Test
  public void testLCS() {
    LongestCommonSubSequencer lcs = new LongestCommonSubSequencer();
    lcs.compareString("the similarity is in the middle", "is in the");
    assertEquals("isinthe", lcs.getLCS());
  }

  @Test
  public void testSimpleLCS() {
    LongestCommonSubSequencer lcs = new LongestCommonSubSequencer();
    lcs.compareString("abc def fdg", " def");
    assertEquals("def", lcs.getLCS());
  }

  @Test
  public void testSimilarityIndex() {
    Double epsilon = 0.001;
    LongestCommonSubSequencer lcs = new LongestCommonSubSequencer();
    lcs.compareString("the similarity is in the middle", "is in the");
    Assert.assertEquals(1, lcs.getSimilarityScore(),epsilon);
  }

  @Test
  public void testFunctionSimilarity() {
    Double epsilon = 0.001;
    PythonFactory pythonFactory = new PythonFactory();
    CustomFile file = new CustomFile(testFilesDir + "Student_test_file_1.py",pythonFactory);
    CustomFile file2 = new CustomFile(testFilesDir + "Student_test2_file_1.py",pythonFactory);
    List<RuleContext> functionList1 = file.getFunctionNodes();
    List<RuleContext> functionList2 = file2.getFunctionNodes();
    LongestCommonSubSequencer lcs = new LongestCommonSubSequencer();
    ASTTravelor astTravelor = pythonFactory.getASTTraveler();
    String function1String = astTravelor.getString(functionList1.get(0));
    String function2String = astTravelor.getString(functionList2.get(0));
    lcs.compareString(function1String, function2String);
    Assert.assertEquals(1, lcs.getSimilarityScore(),epsilon);
  }

  /**
   * Checks if LCS is able identify plagarism if code is
   * simply divided into functions
   */
  @Test
  public void testFunctionSimilarity1() {
    Double epsilon = 0.001;
    PythonFactory pythonFactory = new PythonFactory();
    CustomFile file = new CustomFile(testFilesDir + "Student_test3_file_1.py",pythonFactory);
    CustomFile file2 = new CustomFile(testFilesDir + "Student_test2_file_1.py",pythonFactory);
    List<RuleContext> functionList2 = file2.getFunctionNodes();
    LongestCommonSubSequencer lcs = new LongestCommonSubSequencer();
    ASTTravelor astTravelor = pythonFactory.getASTTraveler();
    String entireFileString = astTravelor.getString(file.getAstRoot());
    String function2String = astTravelor.getString(functionList2.get(0));
    String[] functionSplit = function2String.split("suite",2);
    lcs.compareString(functionSplit[1], function2String);
    Assert.assertEquals(true, lcs.isPlagarised());
  }

  /**
   * Checks if LCS is not reporting false positives.
   */
  @Test
  public void testFunctionSimilarity2() {
    Double epsilon = 0.001;
    PythonFactory pythonFactory = new PythonFactory();
    CustomFile file = new CustomFile(testFilesDir + "Student_test_file_1.py",pythonFactory);
    CustomFile file2 = new CustomFile(testFilesDir + "Student_test2_file_2.py",pythonFactory);
    List<RuleContext> functionList1 = file.getFunctionNodes();
    List<RuleContext> functionList2 = file2.getFunctionNodes();
    LongestCommonSubSequencer lcs = new LongestCommonSubSequencer();
    ASTTravelor astTravelor = pythonFactory.getASTTraveler();
    List<Boolean> isSimilarList = new ArrayList<>();
    List<Boolean> expected = new ArrayList<>();
    for(RuleContext function1: functionList1) {
      for(RuleContext function2: functionList2) {
        String function1String = astTravelor.getString(function1);
        String function2String = astTravelor.getString(function2);
        String functionContent1 = getFunctionContentString(function1String);
        String functionContent2 = getFunctionContentString(function2String);
        lcs.compareString(functionContent1, functionContent2);
        isSimilarList.add(lcs.isPlagarised());
        expected.add(false);
      }
    }
    Assert.assertArrayEquals(expected.toArray(),isSimilarList.toArray());
  }

  private String getFunctionContentString(String functionString) {
    String[] stringArr = functionString.split("suite",2);
    if(stringArr.length == 2) {
      return stringArr[1];
    }
    return functionString;
  }
}
