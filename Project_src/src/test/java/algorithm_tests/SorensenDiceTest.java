package algorithm_tests;

import org.antlr.v4.runtime.RuleContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import org.neu.algorithms.ICompareString;
import org.neu.algorithms.LongestCommonSubSequencer;
import org.neu.algorithms.SorensenDice;
import org.neu.engine.parser.ASTTravelor;
import org.neu.engine.parser.CustomFile;
import org.neu.engine.parser.PythonFactory;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SorensenDiceTest {

  private ICompareString sorensen;

  private static String testFilesDir = "files_for_test/";

  @Before
  public void setUp() {
    sorensen = new SorensenDice();
  }

  @Test
  public void testSorensenDice05Result() {
    sorensen.compareString("this is a test", "awef ief a test");
    assertEquals(0.5, sorensen.getSimilarityScore(), 0.001);
  }

  @Test
  public void testSorensenDice1Result() {
    sorensen.compareString("test1", "test1");
    assertEquals(1, sorensen.getSimilarityScore(), 0.001);
  }

  @Test
  public void testSorensenDice0LengthResult() {
    sorensen.compareString("a", "b");
    assertEquals(0, sorensen.getSimilarityScore(), 0.001);
  }

  @Test
  public void testSorensenDice0Result() {
    sorensen.compareString("test", "bcst");
    assertEquals(0, sorensen.getSimilarityScore(), 0.01);
  }

  @Test
  public void testSorensenDiceIsPlagiarized() {
    sorensen.compareString("test3", "test3");
    assertTrue(sorensen.isPlagarised());
  }

  /**
   * Checks if SorensenDice is able identify plagarism if the order of
   * code is simply shuffled.
   */
  @Test
  public void testFileSimilarity1() {
    Double epsilon = 0.001;
    PythonFactory pythonFactory = new PythonFactory();
    CustomFile file = new CustomFile(testFilesDir + "Student_test4_file_1.py",pythonFactory);
    CustomFile file2 = new CustomFile(testFilesDir + "Student_test5_file_1.py",pythonFactory);
    ASTTravelor astTravelor = pythonFactory.getASTTraveler();
    String entireFileString = astTravelor.getString(file.getAstRoot());
    String entireFileString2 = astTravelor.getString(file2.getAstRoot());
    sorensen.compareString(entireFileString2, entireFileString);
    Assert.assertEquals(true, sorensen.isPlagarised());
  }

  /**
   * Checks if SorensenDice is not reporting false positive
   */
  @Test
  public void testFunctionSimilarity2() {
    Double epsilon = 0.001;
    PythonFactory pythonFactory = new PythonFactory();
    CustomFile file = new CustomFile(testFilesDir + "Student_test_file_1.py",pythonFactory);
    CustomFile file2 = new CustomFile(testFilesDir + "Student_test2_file_2.py",pythonFactory);
    List<RuleContext> functionList1 = file.getFunctionNodes();
    List<RuleContext> functionList2 = file2.getFunctionNodes();
    ASTTravelor astTravelor = pythonFactory.getASTTraveler();
    List<Boolean> isSimilarList = new ArrayList<>();
    List<Boolean> expected = new ArrayList<>();
    for(RuleContext function1: functionList1) {
      for(RuleContext function2: functionList2) {
        String function1String = astTravelor.getString(function1);
        String function2String = astTravelor.getString(function2);
        String functionContent1 = getFunctionContentString(function1String);
        String functionContent2 = getFunctionContentString(function2String);
        sorensen.compareString(functionContent1, functionContent2);
        isSimilarList.add(sorensen.isPlagarised());
        expected.add(false);
      }
    }
    Assert.assertArrayEquals(expected.toArray(),isSimilarList.toArray());
  }

  private String getFunctionContentString(String functionString) {
    String[] stringArr = functionString.split("suite",2);
    if(stringArr.length == 2) {
      return stringArr[1];
    }
    return functionString;
  }
}
