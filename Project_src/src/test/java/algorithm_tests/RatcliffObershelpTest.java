package algorithm_tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.antlr.v4.runtime.RuleContext;
import org.junit.Assert;
import org.junit.Test;
import org.neu.algorithms.ICompareString;
import org.neu.algorithms.RatcliffObershelp;
import org.neu.engine.parser.ASTTravelor;
import org.neu.engine.parser.CustomFile;
import org.neu.engine.parser.PythonFactory;

import java.util.ArrayList;
import java.util.List;

public class RatcliffObershelpTest {

  private static String testFilesDir = "files_for_test/";

  /**
   * Test verifying RatcliffObershelp Class
   * for same string;
   */
  @Test
  public void testSimilarity() {
    ICompareString iCompareString = new RatcliffObershelp();
    iCompareString.compareString("method declaration", "method declaration");
    float result = iCompareString.getSimilarityScore();
    assertEquals(1.0, result, 0.2);
  }

  /**
   * Test verifying RatcliffObershelp Class
   * for reverse string but same tokens;
   */
  @Test
  public void testSimilarity1() {
    ICompareString iCompareString = new RatcliffObershelp();
    iCompareString.compareString("method declaration", "declaration method");
    float result = iCompareString.getSimilarityScore();
    assertEquals(1.0, result, 0.2);
  }

  @Test
  public void testSimilarity2() {
    ICompareString iCompareString = new RatcliffObershelp();
    iCompareString.compareString("method declaration", "method assignment");
    float result = iCompareString.getSimilarityScore();

    assertEquals(0.40, result, 0.2);

  }

  /**
   * Test verifying RatcliffObershelp Class
   * isPlagarised() method;
   */
  @Test
  public void testSimilarity3() {
    ICompareString iCompareString = new RatcliffObershelp();
    iCompareString.compareString("method declaration", "declaration method");
    assertEquals(true,iCompareString.isPlagarised());
  }

  /**
   * Test verifying RatcliffObershelp Class
   * isPlagarised() method;
   */
  @Test
  public void testSimilarity4() {
    ICompareString iCompareString = new RatcliffObershelp();
    iCompareString.compareString("simple_stmt expr_stmt atom atom dictorsetmaker atom arith_expr atom atom atom atom simple_stmt return_stmt atom",
            "simple_stmt return_stmt comparison atom_expr atom trailer comp_op atom comp_op atom_expr atom trailer");
    assertEquals(false,iCompareString.isPlagarised());
  }

  /**
   * Checks if RatcliffObershelp is not reporting false positive
   */
  @Test
  public void testFunctionSimilarity2() {
    Double epsilon = 0.001;
    PythonFactory pythonFactory = new PythonFactory();
    ICompareString iCompareString = new RatcliffObershelp();
    CustomFile file = new CustomFile(testFilesDir + "Student_test_file_1.py",pythonFactory);
    CustomFile file2 = new CustomFile(testFilesDir + "Student_test2_file_2.py",pythonFactory);
    List<RuleContext> functionList1 = file.getFunctionNodes();
    List<RuleContext> functionList2 = file2.getFunctionNodes();
    ASTTravelor astTravelor = pythonFactory.getASTTraveler();
    List<Boolean> isSimilarList = new ArrayList<>();
    List<Boolean> expected = new ArrayList<>();
    for(RuleContext function1: functionList1) {
      for(RuleContext function2: functionList2) {
        String function1String = astTravelor.getString(function1);
        String function2String = astTravelor.getString(function2);
        String functionContent1 = getFunctionContentString(function1String);
        String functionContent2 = getFunctionContentString(function2String);
        iCompareString.compareString(functionContent1, functionContent2);
        isSimilarList.add(iCompareString.isPlagarised());
        expected.add(false);
      }
    }
    Assert.assertArrayEquals(expected.toArray(),isSimilarList.toArray());
  }

  private String getFunctionContentString(String functionString) {
    String[] stringArr = functionString.split("suite",2);
    if(stringArr.length == 2) {
      return stringArr[1];
    }
    return functionString;
  }
}
