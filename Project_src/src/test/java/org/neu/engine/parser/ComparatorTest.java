package org.neu.engine.parser;

import org.bson.types.ObjectId;
import org.junit.Assert;
import org.junit.Test;
import org.neu.mvc.models.Results;
import org.neu.mvc.models.Submission;

import java.util.ArrayList;
import java.util.List;

/**
 * Test for verifying Comparator Class
 */
public class ComparatorTest {

    private static String testFilesDir = "files_for_test/";

    /**
     * Test verifying Comparator is comparing all
     * files and storing the result.
     */
    @Test
    public void testComparatorFileComparison() {
        Submission submissions1 = new Submission(ObjectId.get());
        List<String> fileList = new ArrayList<>();
        fileList.add(testFilesDir + "Student_test_file_1.py");
        fileList.add(testFilesDir + "Student_test_file_2.py");
        submissions1.setFileNames(fileList);
        AbstractFactory pythonFactory = new PythonFactory();
        submissions1.generateCustomFile(pythonFactory);
        Submission submissions2 = new Submission(ObjectId.get());
        List<String> fileList2 = new ArrayList<>();
        fileList2.add(testFilesDir + "Student_test2_file_1.py");
        fileList2.add(testFilesDir + "Student_test2_file_2.py");
        submissions2.setFileNames(fileList2);
        submissions2.generateCustomFile(pythonFactory);
        Comparator comparator = new Comparator(pythonFactory);
        Results results = comparator.compareSubmission(submissions1,submissions2);
        Assert.assertEquals(4,results.getFileComparisonResults().size());
    }
}
