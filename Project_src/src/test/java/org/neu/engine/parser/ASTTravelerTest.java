package org.neu.engine.parser;

import org.antlr.v4.runtime.RuleContext;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Testing ASTTraveler Class
 */
public class ASTTravelerTest {

    private static String testFilesDir = "files_for_test/";
    /**
     * Test the Stringify capability of ASTTraveler
     */
    @Test
    public void testASTTravelerStringify() {
        PythonFactory pythonFactory = new PythonFactory();
        CustomFile file = new CustomFile(testFilesDir + "Student_test_file_1.py",pythonFactory);
        List<RuleContext> functionList = file.getFunctionNodes();
        RuleContext functionNode = functionList.get(0);
        ASTTravelor astTravelor = pythonFactory.getASTTraveler();
        String treeString = astTravelor.getString(functionNode);
        String expected = "funcdef parameters typedargslist tfpdef tfpdef tfpdef suite simple_stmt " +
                "expr_stmt atom atom dictorsetmaker atom arith_expr atom atom atom atom simple_stmt" +
                " return_stmt atom ";
        Assert.assertEquals(expected,treeString);
    }
}
