package org.neu.engine.parser;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Test to verify FileComparisonResult Class
 */
public class FileComparisonResultTest {

    /**
     * Testing storing accuracy of FileComparisonResult Class.
     */
    @Test
    public void testFileComparisonClass() {
        FileComparisonResult fileComparisonResult = new FileComparisonResult("Student_test_file_1.py","Student_test_file_2.py");
        LineNumberPair lineNumberPair = new LineNumberPair(3,7);
        LineNumberPair lineNumberPair1 = new LineNumberPair(15,18);
        LineNumberPair lineNumberPair2 = new LineNumberPair(17,29);
        LineNumberPair lineNumberPair3 = new LineNumberPair(5,15);
        List<LineNumberPair> lineNumberPairs = new ArrayList<>();
        lineNumberPairs.add(lineNumberPair);
        lineNumberPairs.add(lineNumberPair2);
        List<LineNumberPair> lineNumberPairs1 = new ArrayList<>();
        lineNumberPairs1.add(lineNumberPair1);
        lineNumberPairs1.add(lineNumberPair3);
        fileComparisonResult.setFile1LineNumber(lineNumberPairs);
        fileComparisonResult.setFile2LineNumber(lineNumberPairs1);
        Assert.assertEquals(2,fileComparisonResult.getFile1LineNumber().size());
        Assert.assertEquals(2,fileComparisonResult.getFile2LineNumber().size());
    }
}
