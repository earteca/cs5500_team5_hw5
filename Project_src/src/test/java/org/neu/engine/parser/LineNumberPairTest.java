package org.neu.engine.parser;

import org.junit.Assert;
import org.junit.Test;

/**
 * Tests to verify LineNumberPair class
 */
public class LineNumberPairTest {
    /**
     * Verify LineNumberPair class storage accuracy.
     */
    @Test
    public void testLineNumber1() {
        LineNumberPair lineNumberPair = new LineNumberPair(3,7);
        Assert.assertEquals(3,lineNumberPair.getStartLineNumber());
    }

    /**
     * Verify LineNumberPair class storage accuracy.
     */
    @Test
    public void testLineNumber2() {
        LineNumberPair lineNumberPair = new LineNumberPair(3,7);
        Assert.assertEquals(7,lineNumberPair.getLastLineNumber());
    }
}
