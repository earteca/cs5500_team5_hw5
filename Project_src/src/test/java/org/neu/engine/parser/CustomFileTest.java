package org.neu.engine.parser;

import org.antlr.v4.runtime.RuleContext;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import java.util.List;


public class CustomFileTest {

    private static String testFilesDir = "files_for_test/";
    /**
     * Checking whether File is picking all function nodes.
     */
    @Test
    public void testFunctionNodes() {
        PythonFactory pythonFactory = new PythonFactory();
        CustomFile file = new CustomFile(testFilesDir + "Student_test_file_1.py",pythonFactory);
        List<RuleContext> functionList = file.getFunctionNodes();
        assertEquals(1,functionList.size());
    }

    /**
     * Checking whether File is picking all function nodes.
     */
    @Test
    public void testFunctionNodesInAnotherFile() {
        PythonFactory pythonFactory = new PythonFactory();
        CustomFile file = new CustomFile(testFilesDir + "Student_test2_file_1.py",pythonFactory);
        List<RuleContext> functionList = file.getFunctionNodes();
        assertEquals(1,functionList.size());
    }

    /**
     * Checking startLineNumber functionality of File.
     */
    @Test
    public void testStartLineNumber() {
        PythonFactory pythonFactory = new PythonFactory();
        CustomFile file = new CustomFile(testFilesDir + "Student_test2_file_1.py",pythonFactory);
        List<RuleContext> functionList = file.getFunctionNodes();
        assertEquals(1,file.getStartLineNumber(functionList.get(0)));
    }

    /**
     * Checking lastLineNumber functionality of File.
     */
    @Test
    public void testLastLineNumber() {
        PythonFactory pythonFactory = new PythonFactory();
        CustomFile file = new CustomFile(testFilesDir + "Student_test2_file_1.py",pythonFactory);
        List<RuleContext> functionList = file.getFunctionNodes();
        assertEquals(6,file.getLastLineNumber(functionList.get(0)));
    }
}
