package org.neu.engine.parser;

import org.bson.types.ObjectId;
import org.junit.Assert;
import org.junit.Test;
import org.neu.mvc.models.Submission;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to test Submission Class
 */
public class SubmissionTest {

    private static String testFilesDir = "files_for_test/";
    /**
     * Testing generateCustomFiles() method of Submission;
     */
    @Test
    public void testSubmission() {
        Submission submissions1 = new Submission(ObjectId.get());
        List<String> fileList = new ArrayList<>();
        fileList.add(testFilesDir + "Student_test_file_1.py");
        fileList.add(testFilesDir + "Student_test_file_2.py");
        submissions1.setFileNames(fileList);
        AbstractFactory pythonFactory = new PythonFactory();
        submissions1.generateCustomFile(pythonFactory);
        List<CustomFile> customFiles = submissions1.getCustomFilesList();
        Assert.assertEquals(2,customFiles.size());
    }
}
