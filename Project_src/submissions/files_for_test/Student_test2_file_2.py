import json
import time
import datetime
from collections import defaultdict

class Parser:

    def __init__(self):
        self.user_map = defaultdict(lambda : defaultdict(lambda :0))
        self.start_date = start_date
        self.end_date = end_date
        self.all_events_set = set([])

    def parese_file(self,file_name):
        self.file_name = file_name
        self.events = []
        with open(self.file_name) as json_file:
            for line in json_file:
                temp_data = json.loads(line)
                self.events.append(temp_data)
        self.start_date_timestamp = self.convert_to_timestamp(self.start_date)
        self.end_date_timestamp = self.convert_to_timestamp(self.end_date)
        self.create_map()

    def convert_to_timestamp(self,date):
        return time.mktime(datetime.datetime.strptime(date,"%d/%m/%Y").timetuple())

    def is_correct_time(self,timestamp):
        return self.start_date_timestamp <= timestamp <= self.end_date_timestamp

    def create_map(self):
        for event in self.events:
            user_id = event['user_id']
            time_stamp = float(event['event_timestamp'])/1000000 #Converting timestamp to second
            event_name = event["event_name"]
            self.all_events_set.add(event_name)
            if(self.is_correct_time(time_stamp)):
                self.user_map[user_id][event_name]+=1
        self.create_csv()

    def create_header(self,file):
        line = "UserId"
        for event in self.all_events_set:
            line+=","+event
        file.write(line+"\n")

    def create_csv(self):
        csv_file = open("output.csv","w")
        self.create_header(csv_file)
        for user in self.user_map:
            event_map = self.user_map[user]
            line = user
            for event in self.all_events_set:
                line+=","+str(event_map[event])
            csv_file.write(line+"\n")
        csv_file.close()
